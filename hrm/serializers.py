from datetime import datetime
from typing import List, Optional, Literal
from pydantic import BaseModel, EmailStr
from users.serializers import UserModel, RoleModelMatrix


class EmployeeModel(BaseModel):
    id: int
    first_name: str
    middle_name: str
    last_name: str
    birth_date:  datetime
    gender: Literal['Male', 'Female']
    hire_date: datetime
    manager_id: Optional[int]
    gross_salary: float
    user_id: int

    class Config:
        orm_mode = True


class EmployeePostModel(BaseModel):
    first_name: str
    middle_name: str
    last_name: str
    birth_date:  datetime
    gender: Literal['Male', 'Female']
    hire_date: datetime
    manager_id: Optional[int]
    gross_salary: float
    user_id: int

    class Config:
        orm_mode = True


class BanksModel(BaseModel):
    id: int
    name: str
    active: bool

    class Config:
        orm_mode = True


class BanksPostModel(BaseModel):
    name: str

    class Config:
        orm_mode = True


class BanksPatchModel(BaseModel):
    id: int
    name: str

    class Config:
        orm_mode = True


class BanksModelMatrix(BaseModel):
    name: str

    class Config:
        orm_mode = True


class BankDetailsModel(BaseModel):
    id: int
    employee_id: int
    bank_id: int
    bank_name: BanksModelMatrix
    account_number: int

    class Config:
        orm_mode = True


class BankDetModel(BaseModel):
    id: int
    bank_name: BanksModelMatrix
    account_number: str

    class Config:
        orm_mode = True


class BankDetailsPostModel(BaseModel):
    id: int
    employee_id: int
    bank_id: int
    account_number: int

    class Config:
        orm_mode = True


class JobDescriptionModel(BaseModel):
    id: int
    role_id: int
    task: str
    task_description: str
    task_role: RoleModelMatrix

    class Config:
        orm_mode = True

class JobDescriptionPatchModel(BaseModel):
    id: int
    task: str
    task_description: str
  
    class Config:
        orm_mode = True

class JobDescriptionPostModel(BaseModel):
    role_id: int
    task: str
    task_description: str
   
    class Config:
        orm_mode = True


class JobRoleModel(BaseModel):
    id: Optional[int]
    name: Optional[str]
    description: Optional[str]
    job_description: Optional[List[JobDescriptionModel]] = []
    task_role: Optional[RoleModelMatrix]

    class Config:
        orm_mode = True


class HrFileTypeModel(BaseModel):
    id: int
    name: str
    active: bool
    description: str
    class Config:
        orm_mode = True


class LeaveConfigModel(BaseModel):
    id: int 
    name: str
    days: float
    class Config:
        orm_mode = True

class LeaveConfigPostModel(BaseModel):
    name: str
    days: float
    class Config:
        orm_mode = True


class SetLeaveModel(BaseModel):
    year : int
    leave_type: int



class SetSpecficLeaveModel(BaseModel):
    year : int
    leave_type: int
    employee_id: int


class EmployeeLeaveModel(BaseModel):
    id: int
    days: float 
    balance: float 
    leave_year: int 
    status: bool 
    leave_name: Optional[LeaveConfigModel] 

    class Config:
        orm_mode = True

class LeaveRequestModel(BaseModel):
    id: int
    leave_id: int
    start_date: datetime
    requested_days: float
    status: str 
    partial: Optional[float]

    class Config:
        orm_mode = True


class LeaveRequestPostModel(BaseModel):
    leave_id: int
    start_date: datetime
    requested_days: float


class AddressModel(BaseModel):
    id: int
    employee_id: int
    city: str
    phone_number: int
    kebele: int
    woreda:int 
    p_o_box: str

    class Config:
        orm_mode = True


class AddressPostModel(BaseModel):
    employee_id: int
    city: str
    phone_number: int
    kebele: int
    woreda:int 
    p_o_box: str

    class Config:
        orm_mode = True


class HrFileTypePostModel(BaseModel):
    name: str
    description: str

    class Config:
        orm_mode = True


class HrDocumentsModel(BaseModel):
    id: int
    file_name: str
    employee_id: int
    document_type: int
    doc_name: HrFileTypePostModel

    class Config:
        orm_mode = True



class HrDocumentsPostModel(BaseModel):
    employee_id: int
    document_type: int
    
    class Config:
        orm_mode = True


class EmailMessageModel(BaseModel):
    value: str
    subject: str
    email: List[EmailStr]
    text_message: str