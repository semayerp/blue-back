from . import *

@hr_app.get('/job_description/{job_id}', response_model=JobDescriptionPostModel, dependencies=[Depends(get_current_user)])
async def job_description_get(job_id: int, session: AsyncSession = Depends(get_session)):
    try:
        job_desc = await session.execute(select(JobDescription).where(JobDescription.id == job_id).options(selectinload(JobDescription.task_role)))
        job_desc = job_desc.unique().scalars().first()
        return job_desc
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()
            

@hr_app.get('/job_description/{role_id}', response_model=List[JobDescriptionPatchModel], dependencies=[Depends(get_current_user)])
async def role_job_description_get(role_id: int, session: AsyncSession = Depends(get_session)):
    try:
        job_descs = await session.execute(select(JobDescription).where(JobDescription.role_id == role_id))
        job_descs = job_descs.unique().scalars().all()
        return job_descs
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()
    

@hr_app.post('/job_description/', response_model=JobDescriptionPostModel, dependencies=[Depends(get_current_user)])
async def job_description_post(job_desc: JobDescriptionPostModel, session: AsyncSession = Depends(get_session)):
    try:
        session.add(JobDescription(job_desc.dict()))
        await session.commit()
        return job_desc
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.patch('/job_description/{job_id}', response_model=JobDescriptionPatchModel, dependencies=[Depends(get_current_user)])
async def job_description_patch(job_desc:JobDescriptionPatchModel, session: AsyncSession = Depends(get_session)):
    try:
        await session.execute(update(JobDescription).where(JobDescription.id == job_desc.id).values(**job_desc.dict()))
        await session.commit()
        return job_desc
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.delete('/job_description/{job_id}', response_model=JobDescriptionPatchModel, dependencies=[Depends(get_current_user)])
async def job_description_delete(job_id: int, session: AsyncSession = Depends(get_session)):
    try:
        await session.execute(delete(JobDescription).where(JobDescription.id == job_id))
        await session.commit()
        return JSONResponse({"Message" : "Entry deleted successfully"},status.HTTP_202_ACCEPTED)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()
