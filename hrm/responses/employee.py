from . import *
from opencelery.core import send_employee_email


@hr_app.get('/employee', response_model=Page[EmployeeModel])
async def employees_get(session: AsyncSession = Depends(get_session)):
    try:
        employees = select(Employee)
        return await paginate(session, employees)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.post('/employee', response_model=EmployeeModel, dependencies=[Depends(get_current_user)])
async def employee_post(post_employee: EmployeePostModel, session: AsyncSession = Depends(get_session)):
    try:
        new_employee = Employee(** post_employee.dict())
        session.add(new_employee)
        await session.commit()
        return new_employee
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.get('/employee/{employee_id}/', response_model=EmployeeModel, dependencies=[Depends(get_current_user)])
async def single_employee_get(employee_id: int, session: AsyncSession = Depends(get_session)):
    try:
        single_employee = await session.execute(select(Employee).where(Employee.id == employee_id))
        single_employee = single_employee.unique().scalars().first()
        return single_employee
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.post('/employee/mail', dependencies=[Depends(get_current_user)])
async def employee_send_email(email_Message: EmailMessageModel, session: AsyncSession = Depends(get_session)):
    try:
        send_employee_email.apply_async((email_Message,),
                                    queue='opencelery')
        return JSONResponse({"Message": "Please check your email"}, status_code=status.HTTP_202_ACCEPTED)
    except Exception as e:
        
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()
