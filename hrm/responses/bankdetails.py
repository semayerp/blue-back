from . import *


@hr_app.get('/bankdetail/{employee_id}', response_model=List[BankDetModel], dependencies=[Depends(get_current_user)])
async def bank_details_get(employee_id: int, session: AsyncSession = Depends(get_session)):
    try:
        banks = await session.execute(select(BankDetails).where(BankDetails.employee_id == employee_id).options(
                                        selectinload(BankDetails.bank_name)))
        banks = banks.scalars().unique().all()
        banks = [BankDetModel.from_orm(x).dict() for x in banks]
        return banks
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.post('/bankdetail/', response_model=BankDetailsPostModel, dependencies=[Depends(get_current_user)])
async def bank_details_post(bank: BankDetailsPostModel, session: AsyncSession = Depends(get_session)):
    try:
        bank_check = await session.execute(select(Banks).where(Banks.id == bank.bank_id))
        bank_check = bank_check.unique().scalars().first()
        if bank_check.active:
            new_detail = bank.dict()
            new_detail = BankDetails(**new_detail)
            session.add(new_detail)
            await session.commit()
            return new_detail
        return JSONResponse({"detail": "Bank In Use is no longer Active"}, status_code=status.HTTP_412_PRECONDITION_FAILED)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.delete('/bankdetail/{detail_id}', response_model=BankDetailsPostModel, dependencies=[Depends(get_current_user)])
async def bank_details_delete(detail_id: int, session: AsyncSession = Depends(get_session)):
    try:
        detail = await session.execute(select(BankDetails).where(BankDetails.id == detail_id))
        detail = detail.unique().scalars().first()
        await session.execute(delete(BankDetails).where(BankDetails.id == detail_id))
        await session.commit()
        return detail
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()
