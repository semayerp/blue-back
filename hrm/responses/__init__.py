import os
import fitz
import shutil
from uuid import uuid4, UUID
from typing import Any, List, Literal, Annotated
from datetime import datetime, timedelta, timezone
from random import randint
from json import loads
from main.db import get_session
from users.responses import get_current_user
from users.models import User
from users.serializers import UserModel
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, update, delete, exists
from sqlalchemy.orm import joinedload, subqueryload, selectinload
from fastapi import status, APIRouter, Depends, UploadFile, File
from fastapi.responses import JSONResponse
from fastapi_pagination import Page
from fastapi_pagination.ext.sqlalchemy import paginate
from erplogging import logger
from config import settings
from utils.filestoring import FileStore
from hrm.serializers import HrFileTypeModel, BanksModel,\
    BankDetModel, HrDocumentsModel, JobDescriptionModel,LeaveConfigModel, LeaveConfigPostModel, SetLeaveModel,\
    JobRoleModel, BankDetailsPostModel, EmployeeModel, EmployeePostModel, SetSpecficLeaveModel,EmployeeLeaveModel, \
    BanksPostModel, BanksPatchModel, LeaveRequestPostModel, LeaveRequestModel, JobDescriptionPostModel, \
    JobDescriptionPatchModel, AddressModel, AddressPostModel, HrFileTypePostModel, HrDocumentsPostModel,\
    EmailMessageModel
from hrm.models import HrFileType, LeaveConfig, Banks, Employee, EmployeeLeave, EmployeeAddress, HrDocuments,\
    LeaveRequest, JobDescription, LeaveRequest, BankDetails, LeaveStatus, EmployeeStatus

hr_app = APIRouter()

from hrm.responses.banks import banks_post, banks_get, banks_patch, activate_bank_get, deactivate_bank_get
from hrm.responses.bankdetails import bank_details_get, bank_details_post, bank_details_delete
from hrm.responses.employee import employees_get, employee_post, single_employee_get
from hrm.responses.leave import add_leave_type_get, add_leave_type_post, add_leave_type_patch, add_leave_type_delete, \
    set_leave_post, set_specfic_leave_post, deactivate_leave, activate_leave,get_employee_leave_get, request_leave_post, \
    approve_leave_request, decline_leave_request, cancel_leave_request
from hrm.responses.jobdescription import job_description_get, role_job_description_get, job_description_post, job_description_patch, \
    job_description_delete
from hrm.responses.address import address_get, address_post, address_delete
from hrm.responses.filetype import filetype_get, filetype_post, filetype_activate, filetype_deactivate, filetype_dropdown
from hrm.responses.documents import documents_get, documents_post