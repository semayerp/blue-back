from . import *


@hr_app.get('/address/{employee_id}', response_model=List[AddressModel])
async def address_get(employee_id: int, session: AsyncSession = Depends(get_session)):
    try:
        employe_adress = await session.execute(select(EmployeeAddress).where(EmployeeAddress.employee_id == employee_id))
        employe_adress = employe_adress.scalars().unique().all()
        return employe_adress
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.post('/address/', response_model=AddressPostModel)
async def address_post(new_address: AddressPostModel, session: AsyncSession = Depends(get_session)):
    try:
        session.add(EmployeeAddress(new_address.dict()))
        await session.commit()
        return new_address
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.delete('/address/{address_id}')
async def address_delete(address_id: int, session: AsyncSession = Depends(get_session)):
    try:
        await session.execute(delete(EmployeeAddress).where(EmployeeAddress.id == address_id))
        await session.commit()
        return JSONResponse({"Message": "Deleting Object Successful"}, status_code=status.HTTP_202_ACCEPTED)
    except Exception as e:
        logger.error(str(e), exc_info=True)
        await session.rollback()
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()
