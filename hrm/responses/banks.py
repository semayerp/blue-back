from . import *


@hr_app.get('/bank', response_model=Page[BanksModel], dependencies=[Depends(get_current_user)])
async def banks_get(session: AsyncSession = Depends(get_session)):
    try:
        curr_banks = select(Banks)
        return await paginate(session, curr_banks)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.post('/bank', response_model=BanksPostModel, dependencies=[Depends(get_current_user)])
async def banks_post(bank: BanksPostModel, session: AsyncSession = Depends(get_session)):
    try:
        new_bank = Banks(**bank.dict())
        session.add(new_bank)
        await session.commit()
        return new_bank
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.patch('/bank', response_model=BanksPostModel, dependencies=[Depends(get_current_user)])
async def banks_patch(bank: BanksPatchModel, session: AsyncSession = Depends(get_session)):
    try:
        await session.execute(update(Banks).where(Banks.id == bank.id).values(**bank.dict()))
        await session.commit()
        return bank
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.get('/bank_activate/{bank_id}', dependencies=[Depends(get_current_user)])
async def activate_bank_get(bank_id: int, session: AsyncSession = Depends(get_session)):
    try:
        await session.execute(update(Banks).where(Banks.id == bank_id).values(active=True))
        await session.commit()
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.get('/bank_deactivate/{bank_id}', dependencies=[Depends(get_current_user)])
async def deactivate_bank_get(bank_id: int, session: AsyncSession = Depends(get_session)):
    try:
        await session.execute(update(Banks).where(Banks.id == bank_id).values(active=False))
        await session.commit()
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()
