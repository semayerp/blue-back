from . import *


@hr_app.get('/leave_type', response_model=Page[LeaveConfigModel], dependencies=[Depends(get_current_user)])
async def add_leave_type_get(session: AsyncSession = Depends(get_session)):
	try:
		leave_type = select(LeaveConfig)
		return await paginate(session, leave_type)
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()



@hr_app.post('/leave_type', response_model=LeaveConfigPostModel, dependencies=[Depends(get_current_user)])
async def add_leave_type_post(leave_type: LeaveConfigPostModel, session: AsyncSession = Depends(get_session)):                
	try:
		new_leave_type = LeaveConfig(**leave_type.dict())
		session.add(new_leave_type)
		await session.commit()
		return new_leave_type
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()
   

@hr_app.patch('/leave_type', response_model=LeaveConfigPostModel, dependencies=[Depends(get_current_user)])
async def add_leave_type_patch(leave_type: LeaveConfigModel, session: AsyncSession = Depends(get_session)):    
	try:
		await session.execute(update(LeaveConfig).where(LeaveConfig.id == leave_type.id).values(**leave_type.dict()))
		await session.commit()
		return leave_type
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()

	

@hr_app.delete('/leave_type/{leave_type_id}', dependencies=[Depends(get_current_user)])
async def add_leave_type_delete(leave_type_id: int, session: AsyncSession = Depends(get_session)):  	
	try:
		await session.execute(delete(LeaveConfig).where(LeaveConfig.id == leave_type_id))
		await session.execute(delete(EmployeeLeave).where(EmployeeLeave.leave_config_id == leave_type_id))
		await session.commit()
		return  JSONResponse({"Message": "Operation Completed successfully"}, status_code=status.HTTP_200_OK)
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()
   

@hr_app.post('/setleave', dependencies=[Depends(get_current_user)])
async def set_leave_post(setLeave: SetLeaveModel, session: AsyncSession = Depends(get_session)):
	try:
		employees = await session.execute(select(Employee.id)).scalars().unique().all()
		days = await session.execute(select(LeaveConfig).where(LeaveConfig.id == setLeave.leave_type)).scalars().unique().first()         
		days = days.days
		# await session.execute(select(EmployeeLeave.id).where(EmployeeLeave.leave_year == setLeave.year).where(EmployeeLeave.leave_config_id == setLeave.leave_type).where(EmployeeLeave.employee_id == x)).scalar() is None
		dbdata = [EmployeeLeave(employee_id =x,leave_year=setLeave.year,leave_config_id=setLeave.leave_type,days= days ) for x in employees]
		session.add_all(dbdata)
		await session.commit()
		return  JSONResponse({"Message": "Operation Completed successfully"}, status_code=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()

@hr_app.post('/set_specfic_leave', dependencies=[Depends(get_current_user)])
async def set_specfic_leave_post(setSpecfic: SetSpecficLeaveModel, session: AsyncSession = Depends(get_session)):
	try:
		data = session.execute(select(EmployeeLeave.id).where(EmployeeLeave.leave_year == setSpecfic.year).
		       where(EmployeeLeave.leave_config_id == setSpecfic.leave_type).\
			where(EmployeeLeave.employee_id == setSpecfic.employee_id)).scalar()
		days = await session.execute(select(LeaveConfig).where(LeaveConfig.id == setSpecfic.leave_type)).scalars().unique().first()         
		days = days.days
		bdata=[]
		if  data is None:
			dbdata=[EmployeeLeave(employee_id = setSpecfic.employee_id,leave_year=setSpecfic.year,leave_config_id=setSpecfic.leave_type,days=days )] 
		session.add_all(dbdata)
		await session.commit()
		return JSONResponse({"Message": "Operation Completed successfully"}, status_code=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()
          
       
@hr_app.post('/deactivate_leave', dependencies=[Depends(get_current_user)])
async def deactivate_leave(setLeave: SetLeaveModel, session: AsyncSession = Depends(get_session)):
	try:
		session.execute(update(EmployeeLeave).where(EmployeeLeave.leave_year == setLeave.year).where(EmployeeLeave.leave_config_id == setLeave.leave_type)
		  .values(status=False))
		await session.commit()
		return JSONResponse({"Message": "Operation Completed successfully"}, status_code=status.HTTP_202_ACCEPTED)
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()
 

@hr_app.post('/activate_leave', dependencies=[Depends(get_current_user)])
async def activate_leave(setLeave: SetLeaveModel, session: AsyncSession = Depends(get_session)):
	try:
		session.execute(update(EmployeeLeave).where(EmployeeLeave.leave_year == setLeave.year).where(EmployeeLeave.leave_config_id == setLeave.leave_type)
		  .values(status=True))
		await session.commit()
		return JSONResponse({"Message": "Operation Completed successfully"}, status_code=status.HTTP_202_ACCEPTED)
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()
 
 
@hr_app.get('/leave/{employee_id}', response_model=Page[EmployeeLeaveModel], dependencies=[Depends(get_current_user)])
async def get_employee_leave_get(emplyee_id: int, session: AsyncSession = Depends(get_session)): 
	try:
		employee_leave = select(EmployeeLeave).where(EmployeeLeave.id == emplyee_id).options(selectinload(EmployeeLeave.leave_name))
		return await paginate(session, employee_leave)
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()


@hr_app.post('/request_leave/', response_model=LeaveConfigPostModel, dependencies=[Depends(get_current_user)])
async def request_leave_post(leave_request: LeaveRequestPostModel, session: AsyncSession = Depends(get_session)):
	try:
		check_balance = await session.execute(select(EmployeeLeave).where(EmployeeLeave.id == leave_request.leave_id))
		check_balance = check_balance.unique().scalars().first()
		if check_balance.balance >=  leave_request.days:
			session.add(LeaveRequest(**leave_request.dict()))
			await session.commit()
			return leave_request
		return JSONResponse({"Message": "You do not have sufficent Balance"}, status_code=status.HTTP_412_PRECONDITION_FAILED)
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()


    

@hr_app.get('/leave_approve/{request_id}', dependencies=[Depends(get_current_user)])
async def approve_leave_request(request_id: int, session: AsyncSession = Depends(get_session)):
	try:
		session.execute(update(LeaveRequest).where(LeaveRequest.id == request_id).values(status=LeaveStatus.Approved))
		await session.commit()
		return JSONResponse({"Message": "Operation Completed successfully"}, status_code=status.HTTP_202_ACCEPTED)
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()


@hr_app.get('/leave_decline/{request_id}',dependencies=[Depends(get_current_user)])
async def decline_leave_request(request_id: int, session: AsyncSession = Depends(get_session)):
	try:
		session.execute(update(LeaveRequest).where(LeaveRequest.id == request_id).values(status=LeaveStatus.Declined))
		await session.commit()
		return JSONResponse({"Message": "Operation Completed successfully"}, status_code=status.HTTP_202_ACCEPTED)
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()


@hr_app.get('/leave_cancel/{request_id}',dependencies=[Depends(get_current_user)])
async def cancel_leave_request(request_id: int, session: AsyncSession = Depends(get_session)):
	try:
		session.execute(update(LeaveRequest).where(LeaveRequest.id == request_id).values(status=LeaveStatus.Canceled))
		await session.commit()
		return JSONResponse({"Message": "Operation Completed successfully"}, status_code=status.HTTP_202_ACCEPTED)
	except Exception as e:
		await session.rollback()
		logger.error(str(e), exc_info=True)
		return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
	finally:
		await session.close()

