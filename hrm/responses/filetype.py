from . import *


@hr_app.get('/hr_file_type', response_model=Page[HrFileTypeModel])
async def filetype_get(session: AsyncSession = Depends(get_session)):
    try:
        return await paginate(session, select(HrFileType))
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.post('/hr_file_type', response_model=HrFileTypePostModel)
async def filetype_post(new_filetype: HrFileTypePostModel, session: AsyncSession = Depends(get_session)):
    try:
        session.add(HrFileType(**new_filetype.dict()))
        await session.commit()
        return new_filetype
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.patch('/hr_file_type/{file_type_id}')
async def filetype_activate(file_type_id: int, session: AsyncSession = Depends(get_session)):
    try:
        await session.execute(update(HrFileType).where(HrFileType.id == file_type_id).values(active=True))
        await session.commit()
        return JSONResponse({"Message": "Deleting Object Successful"}, status_code=status.HTTP_202_ACCEPTED)
    except Exception as e:
        logger.error(str(e), exc_info=True)
        await session.rollback()
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()



@hr_app.delete('/hr_file_type/{file_type_id}')
async def filetype_deactivate(file_type_id: int, session: AsyncSession = Depends(get_session)):
    try:
        await session.execute(update(HrFileType).where(HrFileType.id == file_type_id).values(active=False))
        await session.commit()
        return JSONResponse({"Message": "Deleting Object Successful"}, status_code=status.HTTP_202_ACCEPTED)
    except Exception as e:
        logger.error(str(e), exc_info=True)
        await session.rollback()
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()

@hr_app.get('/hr_file_type/dropdown', response_model=List[HrFileTypeModel])
async def filetype_dropdown(session: AsyncSession = Depends(get_session)):
    try:
        dropdown = await session.execute(select(HrFileType).where(HrFileType.active == True))
        dropdown = dropdown.scalars().uninque().all()
        return JSONResponse({"Message": "Deleting Object Successful"}, status_code=status.HTTP_202_ACCEPTED)
    except Exception as e:
        logger.error(str(e), exc_info=True)
        await session.rollback()
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()

