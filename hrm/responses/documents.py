from . import *
from opencelery.core import save_pdf

@hr_app.get('/documents/{employee_id}')
async def documents_get(employee_id: int, session: AsyncSession = Depends(get_session)):
    try:
        documents = await session.execute(select(HrDocuments).where(HrDocuments.employee_id == employee_id).options(selectinload(HrDocuments.doc_name)))
        documents = documents.scalars().unique().all()
        douments = [HrDocumentsModel.from_orm(x).dict() for x in documents]
        documents = {
            x.doc_name.name : [y.file_name for y in documents if x.doc_name.name == y.doc_name.name] for x in documents
        }
        return JSONResponse(documents, status_code=status.HTTP_202_ACCEPTED) 
    except Exception as e:
        logger.error(str(e), exc_info=True)
        await session.rollback()
        return JSONResponse({"Message": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.post('/documents/{employee_id}/{type_id}')
async def documents_post(up_file: UploadFile, employee_id: int, type_id: int, session: AsyncSession = Depends(get_session)):
    try:
        employee = await session.execute(select(Employee).where(Employee.id ==employee_id))
        employee = employee.scalars().unique().first()
        doc_type = await session.execute(select(HrFileType).where(HrFileType.id ==type_id))
        doc_type = doc_type.scalars().unique().first()
        docsdir= settings.UPLOAD_FOLDER +'/'+employee.name+'/'+doc_type.name
        if up_file.content_type == "application/pdf" and doc_type and employee:
            temp_name = f"{docsdir}/temp_pdf_file.pdf"
            if not os.path.exists(docsdir):
                os.makedirs(docsdir)
            with open(str(temp_name), "wb") as buffer:
                shutil.copyfileobj(up_file.file, buffer)
            save_pdf.apply_async((docsdir, temp_name, employee.name, employee_id, type_id),
                                  queue='opencelery')
            await session.execute(delete(HrDocuments).where(HrDocuments.employee_id == employee_id).where(HrDocuments.document_type == type_id))
            await session.commit()
            return JSONResponse({"Message": "Working fine"}, status_code=status.HTTP_202_ACCEPTED)
        elif up_file.content_type == "image/png" or up_file.content_type == "image/jpg" or up_file.content_type == "image/jpeg":
            file_name = f"{docsdir}/{employee.name.lower().replace(' ','_')}-{str(randint(1,10000))}.png"
            fs = FileStore()
            fs.store_pic(docsdir,up_file.file,file_name)
            await session.execute(delete(HrDocuments).where(HrDocuments.employee_id == employee_id).where(HrDocuments.document_type == type_id))
            session.add(HrDocuments(employee_id=employee_id, document_type=type_id, file_name=file_name))
            await session.commit()
            return JSONResponse({"Message": "Working fine"}, status_code=status.HTTP_202_ACCEPTED)
        else:
            return JSONResponse({"detail": "File not allowed"}, status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)
    except Exception as e:
        logger.error(str(e), exc_info=True)
        await session.rollback()
        return JSONResponse({"Message": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@hr_app.delete('/documents/{employee_id}/{type_id}')
async def documents_delete(employee_id: int, type_id: int, session: AsyncSession = Depends(get_session)):
    try:
        await session.execute(delete(HrDocuments).where(HrDocuments.employee_id == employee_id).where(HrDocuments.document_type == type_id))
        await session.commit()
    except Exception as e:
        logger.error(str(e), exc_info=True)
        await session.rollback()
        return JSONResponse({"Message": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close
        