import fitz
from abc import ABC, abstractmethod
from PIL import Image,ImageSequence
from os import remove,listdir,makedirs,getcwd
from os.path import exists,join
from random import randint


class FileStore:
    def __init__(self,):
        self.pic_list=None
        self.pdf_list=None
    
    def store_pic(self,docsdir,file,filename):
        try:
            if not exists(docsdir):
                makedirs(docsdir)
            # file_name=docsdir+"/"+filename
            file_name = filename
           
            with Image.open(file) as fm:
                fm.resize((500,300), Image.ANTIALIAS)
                fm.convert('RGB').save(file_name,'png',quality=400)
            self.pic_list=[]
            self.pic_list.append(file_name)
        
        except Exception as e:
            print(e)
    def clean_pic(self):
        try:
            for x in self.pic_list:
                remove(x)
        except Exception as e:
            print(e)
   
    def store_file(self,name_list,filename):   
        try:
            file_name=filename
            file=fitz.open(file_name)
            mat = fitz.Matrix(4,4)
            self.pdf_list=[]
            for i in range(len(file)):
                file.load_page(i).get_pixmap(matrix=mat).save(name_list[i])
                with Image.open(name_list[i]) as fm:
                    fm=fm.resize((round(fm.size[0]*1), round(fm.size[1]*0.75)))
                    fm.convert('RGB').save(name_list[i],'png',quality=400)
                self.pdf_list.append(name_list[i])
            remove(file_name)
        
        except Exception as e:
            print(f"###---> {e}")

    def clean_pdf(self):
        try:
           for x in self.pdf_list:
              remove(x)
        
        except Exception as e:
            print(e)

    def remove_file(self,file_path):
        remove(file_path)