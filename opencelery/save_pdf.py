
# from . import shared_task
# import fitz
# from main.db import sessionmade
# from hrm.models import HrDocuments
# from utils.filestoring import FileStore
# from random import randint


# @shared_task(name="save_pdf", bind=True)
# async def save_pdf(self,docs_dir,temp_name, employee_name, employee_id, type_id):
# 	fs = FileStore()
# 	db_data = []
# 	name_list = []
# 	for i in range(len(fitz.open(temp_name))): 
# 		file_name = f"{docs_dir}/{employee_name.lower().replace(' ','_')}-{str(randint(1,10000))}-{i}.png"
# 		name_list.append(file_name)
# 		db_data.append(HrDocuments(file_name=file_name, employee_id=employee_id, document_type=type_id))
# 	async with sessionmade() as session:
# 		session.add_all(db_data)
# 		await session.commit()
# 	fs.store_file(name_list,temp_name)
