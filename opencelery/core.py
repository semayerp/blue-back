
from . import mail, shared_task
from fastapi_mail import MessageSchema, MessageType
from hrm.serializers import EmailMessageModel

@shared_task(name="send_celery_email", bind=True)
def send_celery_email(self,value, email):
	html = f"""<p>Thanks for using Fastapi-mail {value}</p>"""
	message = MessageSchema(
					subject="OpenERP-Mail module",
					recipients=email,
					body=html,
					subtype=MessageType.html)
	print(message.dict())
	from asyncio import get_event_loop
	get_event_loop().run_until_complete(mail.send_message(message))
	return "Works Exccelent"


# @shared_task(name="reverse", bind=True)
# def reverse(self,myname):
#     return myname[::-1]



@shared_task(name="send_employee_email", bind=True)
def send_employee_email(self, value: EmailMessageModel):
	
	html = f"""<p>Dear {value.value}</p>
            </br>
            <div>
            {value.text_message}
            </div>
            """
	message = MessageSchema(
					subject=value.subject,
					recipients=value.email,
					body=html,
					subtype=MessageType.html)
	print(message.dict())
	from asyncio import get_event_loop
	get_event_loop().run_until_complete(mail.send_message(message))
	return "Works Exccelent"


import fitz
from main.db import sessionmade
from hrm.models import HrDocuments
from utils.filestoring import FileStore
from random import randint


@shared_task(name="save_pdf", bind=True)
async def save_pdf(self,docs_dir,temp_name, employee_name, employee_id, type_id):
	fs = FileStore()
	db_data = []
	name_list = []
	for i in range(len(fitz.open(temp_name))): 
		file_name = f"{docs_dir}/{employee_name.lower().replace(' ','_')}-{str(randint(1,10000))}-{i}.png"
		name_list.append(file_name)
		db_data.append(HrDocuments(file_name=file_name, employee_id=employee_id, document_type=type_id))
	async with sessionmade() as session:
		session.add_all(db_data)
		await session.commit()
	fs.store_file(name_list,temp_name)
