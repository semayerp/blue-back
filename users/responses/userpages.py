from . import *


@useradmin.get('/page', response_model=Page[UsersPageModel],
               dependencies=[Depends(get_current_user)])
async def pages_get(session: AsyncSession = Depends(get_session)):
    try:
        pages = select(SinglePage).options(selectinload(SinglePage.routes)
                                    .subqueryload(RouteResponse.roles))
        logger.info('Pages have been fetched successfully')
        return await paginate(session, pages)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.post('/page', response_model=UsersPageModelPostResponse, dependencies=[Depends(get_current_user)])
async def pages_post(my_page: UsersPageModelPost, session: AsyncSession = Depends(get_session)):
    try:
        page = SinglePage(**my_page.dict())
        session.add(page)
        await session.commit()
        logger.info(f'Page has been created successfully')
        return page
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.patch('/page/{page_id}', response_model=UsersPageModelPostResponse,
                 dependencies=[Depends(get_current_user)])
async def pages_patch(page_id: int, my_page: UsersPageModelPatch, session: AsyncSession = Depends(get_session)):
    try:
        check_page = await session.execute(select(SinglePage).where(SinglePage.id == page_id))
        check_page = check_page.scalars().unique().first()
        if check_page:
            await session.execute(update(SinglePage).where(SinglePage.id == page_id).values(**my_page.dict()))
            await session.commit()
            logger.info(f'Page have been updated successfully')
            return my_page
        return JSONResponse({"detail": "No Such Page"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.delete('/page/{page_id}', response_model=UsersPageModel,
                  dependencies=[Depends(get_current_user)])
async def pages_delete(page_id: int, session: AsyncSession = Depends(get_session)):
    try:
        my_page = await session.execute(select(SinglePage).where(SinglePage.id == page_id))
        my_page = my_page.unique().scalars().first()
        if my_page:
            await session.execute(delete(SinglePage).where(SinglePage.id == page_id))
            await session.execute(delete(PageRoutes).where(PageRoutes.page_id == page_id))
            await session.commit()
            logger.info(f'Path {SinglePage} have been deleted successfully')
            return my_page
        else:
            return JSONResponse({"detail": "Page Not Found"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.post('/page/{page_id}', response_model=RouteResponsePostModel, dependencies=[Depends(get_current_user)])
async def add_pages_route_post(page_id: int, route_id: AddRoute, session: AsyncSession = Depends(get_session)):
    try:
        route = await session.execute(select(RouteResponse).where(RouteResponse.id == route_id.route_id))
        route = route.scalars().unique().first()
        my_page = await session.execute(select(SinglePage).where(SinglePage.id == page_id))
        my_page = my_page.unique().scalars().first()
        if route and my_page:
            data = PageRoutes(route_id=route_id.route_id, page_id=page_id)
            session.add(data)
            await session.commit()
            return route
        return JSONResponse({"detail": "Page or role  Not Found"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.delete('/page/{page_id}/{route_id}', response_model=RouteResponsePostModel,
                  dependencies=[Depends(get_current_user)])
async def add_pages_routes_delete(page_id: int, route_id: int, session: AsyncSession = Depends(get_session)):
    try:
        await session.execute(delete(PageRoutes).where(PageRoutes.page_id == page_id)
                              .where(PageRoutes.route_id == route_id))
        role = await session.execute(select(RouteResponse).where(RouteResponse.id == route_id))
        role = role.scalars().unique().first()
        await session.commit()
        logger.info(f"{role.name} has been successfully removed from Page")
        return role
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.patch('/activatepage/{page_id}', response_model=UsersPageModelPostResponse,
                 dependencies=[Depends(get_current_user)])
async def activate_page(page_id: int, session: AsyncSession = Depends(get_session)):
    try:
        page = await session.execute(select(SinglePage).where(SinglePage.id == page_id))
        page = page.scalars().unique().first()
        if page:
            await session.execute(update(SinglePage).where(SinglePage.id == page_id).values(active=True))
            await session.commit()
            page = await session.execute(select(SinglePage).where(SinglePage.id == page_id))
            page = page.scalars().unique().first()
            return page
        return JSONResponse({"detail": "No Such Page"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.patch('/deactivatepage/{page_id}', response_model=UsersPageModelPostResponse,
                 dependencies=[Depends(get_current_user)])
async def deactivate_page(page_id: int, session: AsyncSession = Depends(get_session)):
    try:
        page = await session.execute(select(SinglePage).where(SinglePage.id == page_id))
        page = page.scalars().unique().first()
        if page:
            await session.execute(update(SinglePage).where(SinglePage.id == page_id).values(active=False))
            await session.commit()
            page = await session.execute(select(SinglePage).where(SinglePage.id == page_id))
            page = page.scalars().unique().first()
            return page
        return JSONResponse({"detail": "No Such Page"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()

