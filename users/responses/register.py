from . import *


@useradmin.post('/register', response_model=UserModel)
async def register(user: UserModelPost, session: AsyncSession = Depends(get_session)):
    try:
        new_user = user.dict()
        new_user['password'] = sha512_hash.hash(new_user['password'],salt_size=16)
        print(new_user)
        new_user = User(**new_user)
        new_user.uid = str(uuid.uuid4())
        session.add(new_user)
        await session.commit()
        return new_user
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.patch('/register/{uid}', response_model=UserModelUpdate, dependencies=[Depends(get_current_user)])
async def register_patch(uid: UUID, user: UserModelUpdate, session: AsyncSession = Depends(get_session)):
    try:
        check_user = await session.execute(select(User).where(User.uid == str(uid)))
        check_user = check_user.scalars().unique().first()
        if check_user:
            await session.execute(update(User).where(User.uid == str(uid)).values(**user.dict()))
            await session.commit()
            return user
        return JSONResponse({"detail": "No Such user"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.delete('/register/{uid}', response_model=UserModel, dependencies=[Depends(get_current_user)])
async def register_user_delete(uid: UUID, session: AsyncSession = Depends(get_session)):
    try:
        check_user = await session.execute(select(User).where(User.uid == str(uid)))
        check_user = check_user.scalars().unique().first()
        user_id = check_user.id
        if check_user:
            await session.execute(delete(User).where(User.uid == str(uid)))
            await session.execute(delete(RolesUsers).where(RolesUsers.user_id == user_id))
            await session.commit()
            return check_user
        return JSONResponse({"detail": "No Such user"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.patch('/activate/{uid}', response_model=UserModel, dependencies=[Depends(get_current_user)])
async def activate_user(uid: UUID, session: AsyncSession = Depends(get_session)):
    try:
        user = await session.execute(select(User).where(User.uid == str(uid)))
        user = user.scalars().unique().first()
        if user:
            await session.execute(update(User).where(User.uid == str(uid)).values(disabled=False))
            await session.commit()
            user = await session.execute(select(User).where(User.uid == str(uid)))
            user = user.scalars().unique().first()
            return user
        return JSONResponse({"detail": "No Such user"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.patch('/deactivate/{uid}', response_model=UserModel, dependencies=[Depends(get_current_user)])
async def deactivate_user(uid: UUID, session: AsyncSession = Depends(get_session)):
    try:
        user = await session.execute(select(User).where(User.uid == str(uid)))
        user = user.scalars().unique().first()
        if user:
            await session.execute(update(User).where(User.uid == str(uid)).values(disabled=True))
            await session.commit()
            user = await session.execute(select(User).where(User.uid == str(uid)))
            user = user.scalars().unique().first()
            return user
        return JSONResponse({"detail": "No Such user"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()

@useradmin.patch('/password/{uid}', response_model=UserModel)
async def reset_password(uid: UUID, session: AsyncSession = Depends(get_session)):
    try:
        user = await session.execute(select(User).where(User.uid == str(uid)))
        user = user.scalars().unique().first()
        if user:
            password = sha512_hash.hash('default@123',salt_size=16)
            await session.execute(update(User).where(User.uid == str(uid)).values(password=password))
            await session.commit()
            user = await session.execute(select(User).where(User.uid == str(uid)))
            user = user.scalars().unique().first()
            return user
        return JSONResponse({"detail": "No Such user"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()
