from . import *


@useradmin.get('/roles', response_model=Page[RoleModelMatrix], dependencies=[Depends(get_current_user)])
async def role_get(session: AsyncSession = Depends(get_session)):
    try:
        logger.info('Role  have been fetched successfully')
        return await paginate(session, select(Role))
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.get('/dropdown', response_model=List[RoleModelMatrix], dependencies=[Depends(get_current_user)])
async def role_get_dropdown(route_id: int = None, uid: UUID = None, page_id: int = None,
                            session: AsyncSession = Depends(get_session)):
    try:
        all_roles = await session.execute(select(Role))
        all_roles = all_roles.unique().scalars().all()
        if route_id:
            user_id = None
            my_roles = await session.execute(select(RouteResponse).where(RouteResponse.id == route_id).
                                             options(selectinload(RouteResponse.roles)))
            my_roles = my_roles.unique().scalars().first()
        elif uid:
            my_roles = await session.execute(select(User).where(User.uid == str(uid)).
                                             options(selectinload(User.roles)))
            my_roles = my_roles.unique().scalars().first()
        else:
            my_roles = all_roles
        logger.info('Role  have been fetched successfully')
        my_roles = [RoleModelMatrix.from_orm(x) for x in all_roles if x not in my_roles.roles]
        return my_roles
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.post('/roles', response_model=RoleModel, dependencies=[Depends(get_current_user)])
async def role_post(role: RoleModel, session: AsyncSession = Depends(get_session)):
    try:
        role = Role(**role.dict())
        session.add(role)
        await session.commit()
        logger.info(f'Role {role} have been created successfully')
        return role
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.patch('/roles', response_model=RoleModel, dependencies=[Depends(get_current_user)])
async def role_patch(role: RoleModelMatrix, session: AsyncSession = Depends(get_session),
                     ):
    try:
        check_role = await session.execute(select(Role).where(Role.id == role.id))
        check_role = check_role.scalars().unique().first()
        if check_role:
            await session.execute(update(Role).where(Role.id == role.id).values(**role.dict()))
            await session.commit()
            logger.info(f'Role {role} have been updated successfully')
            return role
        return JSONResponse({"detail": "No Such Role"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.delete('/roles/{role_id}', response_model=RoleModel, dependencies=[Depends(get_current_user)])
async def role_delete(role_id: int, session: AsyncSession = Depends(get_session)):
    try:
        role = await session.execute(select(Role).where(Role.id == role_id))
        role = role.unique().scalars().first()
        if role:
            await session.execute(delete(Role).where(Role.id == role_id))
            await session.execute(delete(RolesUsers).where(RolesUsers.role_id == role_id))
            await session.execute(delete(RouteResponseRoles).where(RouteResponseRoles.role_id == role_id))
            await session.commit()
            logger.info(f'Role {role} have been deleted successfully')
            return role
        return JSONResponse({"detail": "Role Not Found"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.get('/roles/{uid}', response_model=RoleUserModelAll, dependencies=[Depends(get_current_user)])
async def add_user_role_get(uid: UUID, session: AsyncSession = Depends(get_session)):
    try:
        user = await session.execute(select(User).where(User.uid == str(uid)).options(selectinload(User.roles)))
        user = user.scalars().unique().first()
        if user:
            return user
        return JSONResponse({"detail": "User Not Found"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.post('/roles/{uid}', response_model=RoleModel, dependencies=[Depends(get_current_user)])
async def add_user_role_post(uid: UUID, role_id: AddRole, session: AsyncSession = Depends(get_session)):
    try:
        check_role = await session.execute(select(Role).where(Role.id == role_id.role_id))
        check_role = check_role.scalars().unique().first()
        user = await session.execute(select(User).where(User.uid == str(uid)))
        user = user.scalars().unique().first()
        if check_role and user:
            user_id = user.id
            data = RolesUsers(user_id=user_id, role_id=role_id.role_id)
            session.add(data)
            role = await session.execute(select(Role).where(Role.id == role_id.role_id))
            role = role.scalars().unique().first()
            await session.commit()
            return role
        return JSONResponse({"detail": "User or Role does not Exist"}, status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()


@useradmin.delete('/roles/{uid}/{role_id}', response_model=RoleModel, dependencies=[Depends(get_current_user)])
async def add_user_role_delete(uid: UUID, role_id: int, session: AsyncSession = Depends(get_session)):
    try:
        user = await session.execute(select(User).where(User.uid == str(uid)))
        user = user.scalars().unique().first()
        user_id = user.id
        await session.execute(delete(RolesUsers).where(RolesUsers.user_id == user_id)
                              .where(RolesUsers.role_id == role_id))
        role = await session.execute(select(Role).where(Role.id == role_id))
        role = role.scalars().unique().first()
        await session.commit()
        logger.info(f"{role.name} has been successfully removed from user")
        return role
    except Exception as e:
        await session.rollback()
        logger.error(str(e), exc_info=True)
        return JSONResponse({"detail": str(e)}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    finally:
        await session.close()
