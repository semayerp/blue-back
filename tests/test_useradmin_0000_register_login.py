import uuid
import pytest


special_key_u = None
reftoken_u = None
uid_u = None
uid_for_delete_u = None


@pytest.mark.asyncio
class TestRegisterForTest:
    uid = "something"
    register_user = {
        "email": "beimdegefu@yande2.com",
        "password": "default@123",
        "disabled": False
    }

    async def test_register_user_post(self, client):
        """Test case for register operations _register_post and register_patch
        Register User
        """
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key_u}",
        }
        response = await client.post(
            "/useradmin/register",
            headers=headers,
            json=self.register_user,
        )
        #  check success registration
        global uid_u
        uid_u = response.json()['uid']
        assert response.status_code == 200


@pytest.mark.asyncio
class TestLogin:
    login_user_model = {
        "grant_type": "authorization_code",
        "email": "somemail@some.com",
        "password": "password",
        "token": "none"
    }
    log_in_model_valid = {
        "grant_type": "authorization_code",
        "email": "beimdegefu@yande2.com",
        "password": "default@123",
        "token": "none"
    }

    log_in_model_ref_token = {
        "grant_type": "refresh_token",
        "email": "something@yandex.com",
        "password": "something@123",
        "token": "none"
    }

    log_in_model_token_decode = {
        "grant_type": "token_decode",
        "email": "something@yandex.com",
        "password": "something@123",
        "token": "none"
    }

    async def test_login_user_admin_login_post_authorization_code_invalid_user(self, client):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        }
        response = await client.post(
            "/useradmin/login",
            headers=headers,
            json=self.login_user_model,
        )
        assert response.status_code == 500 or response.status_code == 401

    async def test_login_user_admin_login_post_authorization_code_valid_user(self, client):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        }
        response = await client.post(
            "/useradmin/login",
            headers=headers,
            json=self.log_in_model_valid,
        )
        self.log_in_model_ref_token['token'] = response.json()['refresh_token']
        self.log_in_model_token_decode['token'] = response.json()['access_token']
        global reftoken_u
        global special_key_u
        reftoken_u = response.json()["refresh_token"]
        special_key_u = response.json()["access_token"]
        assert response.status_code == 202

    async def test_login_user_admin_login_post_refresh_token(self, client):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        }
        response = await client.post(
            "/useradmin/login",
            headers=headers,
            json=self.log_in_model_ref_token,
        )
        assert response.status_code == 202

    async def test_login_user_admin_login_post_token_decode(self, client):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        }
        response = await client.post(
            "/useradmin/login",
            headers=headers,
            json=self.log_in_model_token_decode,
        )

        assert response.status_code == 206


@pytest.mark.asyncio
class TestRegister:
    uid_u = "something"
    register_user = {
        "email": "beimdegefu@yande2.com",
        "password": "default@123",
        "disabled": False
    }
    another_user_delete = {
        "email": "crazymail@some.com",
        "password": "default@123",
        "disabled": False
    }
    patch_data = {"email": "beimdegefu@yandex.com",
                  "date_registered": "2023-02-28T08:58:24.743Z"
                  }

    async def test_register_user_post_unique(self, client):
        response = await client.post(
            "/useradmin/register",
            headers={
                'accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": f"Bearer {special_key_u}",
            },
            json=self.register_user,
        )
        #  asserting unique constraint is maintained
        assert response.status_code == 500

    async def test_register_user_patch(self, client):
        # patch operations
        global uid_u
        response = await client.patch(
            f"/useradmin/register/{uid_u}",
            headers={
                'accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": f"Bearer {special_key_u}",
            },
            json=self.patch_data,
        )
        #  asserting patching user is working
        print(response.json())
        assert response.status_code == 200

    async def test_register_user_patch_invalid_uid(self, client):
        response = await client.patch(
            f"/useradmin/register/{str(uuid.uuid4())}",
            headers={
                'accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": f"Bearer {special_key_u}",
            },
            json=self.patch_data,
        )
        #  asserting invalid uuid response
        assert response.status_code == 404

    async def test_register_user_post_for_delete(self, client):
        """Test case for register operations _register_post and register_patch
        Register User
        """
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key_u}",
        }
        response = await client.post(
            "/useradmin/register",
            headers=headers,
            json=self.another_user_delete,
        )
        #  check success registration
        global uid_for_delete_u
        print(f"####\n{response.json()}\n####")
        uid_for_delete_u = response.json()['uid']
        assert response.status_code == 200

    async def test_register_delete_delete_valid(self, client):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key_u}",
        }

        response = await client.delete(
            f"/useradmin/register/{uid_for_delete_u}",
            headers=headers,
        )
        # delete role of user with correct uuid and role id
        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500

    async def test_register_delete_invalid_uuid(self, client):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key_u}",
        }

        response = await client.delete(
            f"/useradmin/register/{str(uuid.uuid4())}",
            headers=headers,
        )
        # delete role of user with wrong uuid
        assert response.status_code == 404 or response.status_code == 401 or response.status_code == 500
