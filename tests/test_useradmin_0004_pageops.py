import pytest


@pytest.mark.asyncio
class TestPageResponse:
    page_id = 1
    page_d_id = 2
    page_post = {
        "name": "working",
        "app": "admin",
        "description": "string"
    }
    page_patch = {
        "id": 1,
        "name": "working two",
        "app": "admin one",
        "description": "change posting changed"
    }
    page_patch_violate_unique = {
        "id": 1,
        "name": "working two",
        "app": "admin two",
        "description": "change posting changed"
    }
    page_post_for_delete = {
        "name": "remove",
        "app": "admin",
        "description": "for string"
    }
    add_route_post = {
        "route_id": 1,
    }
    add_route_post_wrong = {
        "route_id": 25
    }

    async def test_pages_get_invalid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = await client.get(
            "/useradmin/page",
            headers=headers,
        )

        assert response.status_code == 401

    async def test_pages_get_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.get(
            "/useradmin/page",
            headers=headers,
        )

        assert response.status_code == 200 or response.status_code == 401

    async def test_pages_post_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            "/useradmin/page",
            headers=headers,
            json=self.page_post
        )

        assert response.status_code == 200 or response.status_code == 401

    async def test_pages_post_for_delete_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            "/useradmin/page",
            headers=headers,
            json=self.page_post_for_delete
        )

        assert response.status_code == 200 or response.status_code == 401

    async def test_pages_post_invalid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            "/useradmin/page",
            headers=headers,
            json=self.page_post
        )

        assert response.status_code == 500 or response.status_code == 401

    async def test_pages_patch_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.patch(
            f"/useradmin/page/{self.page_id}",
            headers=headers,
            json=self.page_patch
        )

        assert response.status_code == 200 or response.status_code == 401

    async def test_pages_patch_invalid_wrong_page_id(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.patch(
            f"/useradmin/page/{25}",
            headers=headers,
            json=self.page_patch
        )

        assert response.status_code == 404 or response.status_code == 401 or response.status_code == 500

    async def test_pages_patch_invalid_violate_unique(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.patch(
            f"/useradmin/page/{2}",
            headers=headers,
            json=self.page_patch_violate_unique
        )

        assert response.status_code == 404 or response.status_code == 401 or response.status_code == 500

    async def test_pages_delete_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }
        response = await client.delete(
            f"/useradmin/page/{self.page_d_id}",
            headers=headers,
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500

    async def test_pages_delete_invalid_wrong_id(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }
        response = await client.delete(
            f"/useradmin/page/{self.page_d_id}",
            headers=headers,
        )

        assert response.status_code == 404 or response.status_code == 401 or response.status_code == 500

    async def test_add_pages_route_post_valid_page(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            f"/useradmin/page/{self.page_id}",
            headers=headers,
            json=self.add_route_post
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500 \
               or response.status_code == 404

    async def test_add_pages_route_post_invalid_page(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            f"/useradmin/page/{25}",
            headers=headers,
            json=self.add_route_post
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500 \
               or response.status_code == 404

    async def test_add_pages_route_post_post(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            f"/useradmin/page/{self.page_id}",
            headers=headers,
            json=self.add_route_post_wrong
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500 \
            or response.status_code == 404

    async def test_add_pages_route_delete_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.delete(
            f"/useradmin/page/{self.page_id}/{1}",
            headers=headers,
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500

    async def test_add_pages_route_delete_invalid_page_id(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.delete(
            f"/useradmin/page/{25}/{1}",
            headers=headers,
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500

    async def test_add_pages_route_delete_invalid_role_id(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.delete(
            f"/useradmin/page/{self.page_id}/{25}",
            headers=headers,
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500
