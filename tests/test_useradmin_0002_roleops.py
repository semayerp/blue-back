import pytest
import uuid

@pytest.mark.asyncio
class TestAddRoles:
    id = 2
    role_post = {
        "name": "superuser",
        "description": "Developer Role"
    }
    role_post_delete = {
        "name": "will delete",
        "description": "delete"
    }

    role_patch = {
        "id": 1,
        "name": "superuser",
        "description": "Developer Role Updated"
    }
    no_role_patch = {
        "id": 25,
        "name": "standard",
        "description": "Developer Role Updated"
    }
    add_role_post = {
        "role_id": 1,
            }
    add_role_post_wrong = {
          "role_id": 25
            }
    add_role_delete = {
        "role_id": 1,
            }
    add_role_delete_wrong = {
        "role_id": 1,
    }

    async def test_role_get_invalid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = await client.get(
            "/useradmin/roles",
            headers=headers,

        )
        # fetching roles if exist unauthorized
        assert response.status_code == 401

    async def test_role_get_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.get(
            "/useradmin/roles",
            headers=headers,
        )
        # fetching roles if exist unauthorized
        assert response.status_code == 200 or response.status_code == 401

    async def test_role_get_dropdown_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.get(
            "/useradmin/dropdown",
            headers=headers,
        )
        # fetching roles if exist unauthorized
        assert response.status_code == 200 or response.status_code == 401

    async def test_role_post_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            "/useradmin/roles",
            headers=headers,
            json=self.role_post
        )
        # creating roles if with required privileges
        assert response.status_code == 200 or response.status_code == 401

    async def test_role_post_for_delete_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            "/useradmin/roles",
            headers=headers,
            json=self.role_post_delete
        )
        # creating roles if with required privileges
        assert response.status_code == 200 or response.status_code == 401

    async def test_role_post_invalid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            "/useradmin/roles",
            headers=headers,
            json=self.role_post
        )
        # creating roles if with required privileges validating unique
        assert response.status_code == 500 or response.status_code == 401

    async def test_role_patch_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.patch(
            "/useradmin/roles",
            headers=headers,
            json=self.role_patch
        )
        # updating roles
        assert response.status_code == 200 or response.status_code == 401

    async def test_role_patch_invalid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.patch(
            "/useradmin/roles",
            headers=headers,
            json=self.no_role_patch
        )
        # updating role with wrong id
        assert response.status_code == 404 or response.status_code == 401 or response.status_code == 500

    async def test_role_delete_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.delete(
            f"/useradmin/roles/{self.id}",
            headers=headers,

        )
        # delete role with id
        assert response.status_code == 404 or response.status_code == 200 or response.status_code == 500

    async def test_add_user_role_get_valid(self, client, special_key, uid):

        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.get(
            f"/useradmin/roles/{uid}",
            headers=headers,
        )
        # fetching roles of user
        assert response.status_code == 200 or response.status_code == 404 or response.status_code == 500

    async def test_add_user_role_get_invalid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.get(
            f"/useradmin/roles/{str(uuid.uuid4())}",
            headers=headers,
        )
        # fetching roles  of user that does not exist
        assert response.status_code == 200 or response.status_code == 404 or response.status_code == 500

    async def test_add_user_role_post_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            f"/useradmin/roles/{uid}",
            headers=headers,
            json=self.add_role_post
        )
        # creating roles to a user
        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500

    async def test_add_user_role_post_invalid_uidd(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            f"/useradmin/roles/{str(uuid.uuid4())}",
            headers=headers,
            json=self.add_role_post
        )
        # creating roles to user with non-existent value of uuid
        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500 \
               or response.status_code == 404

    async def test_add_user_role_post_invalid_role_id(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            f"/useradmin/roles/{uid}",
            headers=headers,
            json=self.add_role_post_wrong
        )
        # creating roles to user with non-existent value of role id
        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500 \
            or response.status_code == 404

    async def test_add_user_role_delete_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.delete(
            f"/useradmin/roles/{uid}/{self.add_role_delete['role_id']}",
            headers=headers,
        )
        # delete role of user with correct uuid and role id
        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500

    async def test_add_user_role_delete_invalid_uuid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.delete(
            f"/useradmin/roles/{str(uuid.uuid4())}/{self.add_role_delete_wrong['role_id']}",
            headers=headers,
        )
        # delete role of user with wrong uuid
        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500

    async def test_add_user_role_delete_invalid_role_id(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.delete(
            f"/useradmin/roles/{uid}/{self.add_role_delete_wrong['role_id']}",
            headers=headers,
        )
        # delete role of user with wrong role id
        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500

