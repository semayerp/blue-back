import pytest
import uuid
@pytest.mark.asyncio
class TestUserOperations:
    async def test_activate_user_patch(self, client, special_key, uid):
        # patch operations
        response = await client.patch(
            f"/useradmin/activate/{uid}",
            headers={
                'accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": f"Bearer {special_key}",
            },

        )
        #  asserting patching user is working
        assert response.status_code == 200

    async def test_activate_user_patch_invalid(self, client, special_key, uid):
        # patch operations

        response = await client.patch(
            f"/useradmin/activate/{str(uuid.uuid4())}",
            headers={
                'accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": f"Bearer {special_key}",
            },
        )
        #  asserting patching user is working
        assert response.status_code == 404 or response.status_code == 500

    async def test_deactivate_user_patch_valid(self, client, special_key, uid):
        # patch operations
        response = await client.patch(
            f"/useradmin/deactivate/{uid}",
            headers={
                'accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": f"Bearer {special_key}",
            },
        )
        #  asserting patching user is working
        assert response.status_code == 200

    async def test_deactivate_user_patch_invalid(self, client, special_key, uid):
        # patch operations

        response = await client.patch(
            f"/useradmin/deactivate/{str(uuid.uuid4())}",
            headers={
                'accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": f"Bearer {special_key}",
            },
        )
        #  asserting patching user is working
        assert response.status_code == 404 or response.status_code == 500

