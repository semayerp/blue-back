import pytest


@pytest.mark.asyncio
class TestRouteResponse:
    route_id = 1
    route_d_id = 2
    path_post = {
        "name": "adding address",
        "route_path": "address_delete",
        "description": "employee address posting"
    }
    path_patch = {
        "name": "adding address changed",
        "route_path": "address_delete_changed",
        "description": "employee address posting changed"
    }
    path_patch_violate_unique = {
        "name": "adding address changed",
        "route_path": "address_delete_changed",
        "description": "employee address posting changed"
    }
    path_post_for_delete = {
        "name": "adding address",
        "route_path": "address_delete_again",
        "description": "employee address posting"
    }
    add_role_post = {
        "role_id": 1,
    }
    add_role_post_wrong = {
        "role_id": 25
    }

    async def test_response_path_get_invalid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = await client.get(
            "/useradmin/response_path",
            headers=headers,
        )

        assert response.status_code == 401

    async def test_response_path_get_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.get(
            "/useradmin/response_path",
            headers=headers,
        )

        assert response.status_code == 200 or response.status_code == 401

    async def test_response_path_post_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            "/useradmin/response_path",
            headers=headers,
            json=self.path_post
        )

        assert response.status_code == 200 or response.status_code == 401

    async def test_response_path_post_for_delete_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            "/useradmin/response_path",
            headers=headers,
            json=self.path_post_for_delete
        )

        assert response.status_code == 200 or response.status_code == 401

    async def test_response_path_post_invalid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            "/useradmin/response_path",
            headers=headers,
            json=self.path_post
        )

        assert response.status_code == 500 or response.status_code == 401

    async def test_response_path_patch_patch_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.patch(
            f"/useradmin/response_path/{self.route_id}",
            headers=headers,
            json=self.path_patch
        )

        assert response.status_code == 200 or response.status_code == 401

    async def test_response_path_patch_invalid_wrong_route_id(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.patch(
            f"/useradmin/response_path/{25}",
            headers=headers,
            json=self.path_patch
        )

        assert response.status_code == 404 or response.status_code == 401 or response.status_code == 500

    async def test_response_path_patch_invalid_violate_unique(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.patch(
            f"/useradmin/response_path/{2}",
            headers=headers,
            json=self.path_patch_violate_unique
        )

        assert response.status_code == 404 or response.status_code == 401 or response.status_code == 500

    async def test_response_path_delete_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }
        response = await client.delete(
            f"/useradmin/response_path/{self.route_d_id}",
            headers=headers,
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500

    async def test_response_path_delete_invalid_wrong_id(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }
        response = await client.delete(
            f"/useradmin/response_path/{self.route_d_id}",
            headers=headers,
        )

        assert response.status_code == 404 or response.status_code == 401 or response.status_code == 500

    async def test_add_response_path_role_post_valid_route(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            f"/useradmin/response_path/{self.route_id}",
            headers=headers,
            json=self.add_role_post
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500 \
               or response.status_code == 404

    async def test_add_response_path_role_post_invalid_route(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            f"/useradmin/response_path/{25}",
            headers=headers,
            json=self.add_role_post
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500 \
               or response.status_code == 404

    async def test_add_response_path_role_id(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.post(
            f"/useradmin/response_path/{self.route_id}",
            headers=headers,
            json=self.add_role_post_wrong
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500 \
            or response.status_code == 404

    async def test_add_response_path_role_delete_valid(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.delete(
            f"/useradmin/response_path/{self.route_id}/{1}",
            headers=headers,
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500

    async def test_add_response_path_role_delete_invalid_route_id(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.delete(
            f"/useradmin/response_path/{25}/{1}",
            headers=headers,
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500

    async def test_add_response_path_role_delete_invalid_role_id(self, client, special_key, uid):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": f"Bearer {special_key}",
        }

        response = await client.delete(
            f"/useradmin/response_path/{1}/{25}",
            headers=headers,
        )

        assert response.status_code == 200 or response.status_code == 401 or response.status_code == 500
